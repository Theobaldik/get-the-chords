# -*- coding: utf-8 -*-

import requests
import re
import chord

transpose = 5

src = requests.get('https://supermusic.cz/skupina.php?action=piesen&idskupiny=0&idpiesne=1249462').text

raw_text = re.search(r"</SCRIPT>([\s\S^]*?)<script", src)
raw_text = raw_text.group(1).strip()
raw_text = re.sub(r"<br/>|<sup>|</sup>", "", raw_text)

found_list = re.findall(r"<a.*?>.*?</a>", raw_text)

for item in found_list:
    print(item)

for item in found_list:
    chord_found = re.search(r"<a.*?>(.*?)</a>", item)
    chord_found = chord_found.group(1)
    print("chord_found: {}".format(chord_found))
    print("substitute: {}".format("[{}]".format(str(chord.Chord(chord_found).get_transposed(transpose)))))
    final_chord = "[{}]".format(str(chord.Chord(chord_found).get_transposed(transpose)))
    raw_text = raw_text.replace(item, final_chord, 1)  

with open('test1.txt', 'w') as fil:
    fil.write(raw_text)