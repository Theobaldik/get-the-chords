import gui
import supermusic

def main():
    gui_ = gui.GUI()
    gui_.set_generate_command(supermusic.generate_text)
    gui_.run()

main()
