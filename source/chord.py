chords_dict = {
            'C' : 0, 
            'C#' : 1,
            'D' : 2,
            'Es' : 3,
            'E' : 4,
            'F' : 5,
            'F#' : 6,
            'G' : 7,
            'G#' : 8,
            'A' : 9,
            'B' : 10,
            'H' : 11
            }

class Chord:

    def __init__(self, chord_name):
        self._analyze_chord(chord_name)

    def __str__(self):
        return self._tone + self._degree

    def transpose(self, value):
        cycle = 12
        value = value % cycle
        new_value = self._get_value(self._tone) + value
        new_value += cycle
        self._tone = self._get_tone(new_value % cycle)
    
    def get_transposed(self, value):
        cycle = 12
        value = value % cycle
        new_value = self._get_value(self._tone) + value
        new_value += cycle
        return self._get_tone(new_value % cycle) + self._degree

    def _analyze_chord(self, chord_name):
        if len(chord_name) > 1:
            if chord_name[1] == 's' or chord_name[1] == '#':
                self._tone = self._optimize_tone(chord_name[:2])
                self._degree = chord_name[2:]
            else:
                self._tone = chord_name[0]
                self._degree = chord_name[1:]
        else:
            self._tone = chord_name[0]
            self._degree = ""

    def _optimize_tone(self, tone):
        if tone == 'As':
            return 'G#'
        elif tone == 'D#':
            return 'Es'
        else:
            return tone

    def _get_tone(self, value):
        for tone, value_ in chords_dict.items():
            if value_ == value:
                return tone
        return None

    def _get_value(self, tone):
        return chords_dict[tone]

if __name__ == '__main__':
    cmi = Chord('Cmi')
    asmaj = Chord('Asmaj')
    
    print('Cmi: {}'.format(cmi))
    # cmi.transpose(5)
    print('Cmi+5: {}'.format(cmi.get_transposed(5)))

    print('Asmaj: {}'.format(asmaj))
    asmaj.transpose(-5)
    print('Asmaj-5: {}'.format(asmaj))
    