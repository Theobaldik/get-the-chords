import tkinter as tk
import tkinter.ttk as ttk
import textension

class GUI:
    
    def __init__(self):
        self.root = tk.Tk('Get the Song')
        self.root.minsize(380, 650)
        self.root.title('Get the Song')

        self.source_link = tk.StringVar()
        self.transpose_val = tk.IntVar()

        self.generate_command = None
        self.copy_command = None
        self.save_command = None

        self.make_main_frame()

    def run(self):
        self.root.mainloop()

    def set_generate_command(self, command):
        self.generate_command = command

    def set_copy_command(self, command):
        self.copy_command = command

    def set_save_command(self, command):
        self.save_command = command

    def make_main_frame(self):

        border_width = 2
        border_color = '#dadbdc'
        bg_color = '#ffffff'

        main_frame = tk.Frame(self.root, highlightbackground=border_color, highlightthickness=border_width, bg=bg_color)

        upper_frame = tk.Frame(main_frame, highlightbackground=border_color, highlightthickness=border_width, bg=bg_color)
        link_label = tk.Label(upper_frame, text="Link to song:", bg=bg_color)
        link_entry = tk.Entry(upper_frame, textvariable=self.source_link, relief='flat', highlightcolor=border_color, highlightthickness=border_width)
        transpose_label = tk.Label(upper_frame, text="Transpose:", bg=bg_color)
        transpose_scale = tk.Scale(upper_frame, variable=self.transpose_val, orient='horizontal', width=8, length=210, from_=-11, to=11, bg=bg_color, highlightthickness=0)
        generate_button = tk.Button(upper_frame, text="Generate", width=12, command=lambda: self.generate_command(self.source_link.get(), self.transpose_val.get(), preview_text))

        mid_frame = tk.Frame(main_frame, highlightbackground=border_color, highlightthickness=border_width, bg=bg_color)
        preview_text = textension.Textension(mid_frame, relief='flat')
        
        bottom_frame = tk.Frame(main_frame, highlightbackground=bg_color, highlightthickness=border_width, bg=bg_color)
        save_button = tk.Button(bottom_frame, text="Save to file")
        copy_button = tk.Button(bottom_frame, text="Copy to clipboard")

        main_frame.pack(pady=20, padx=20, fill='both', expand=1)
    
        upper_frame.pack(fill='x', padx=10, pady=10)
        link_label.grid(row=0, column=0, sticky='nsw', pady=5, padx=5)
        link_entry.grid(row=0, column=1, sticky='nswe', columnspan=2, pady=5, padx=5)
        transpose_label.grid(row=1, column=0, sticky='sw', pady=5, padx=5)
        transpose_scale.grid(row=1, column=1, sticky='nsw', pady=5, padx=5)
        generate_button.grid(row=1, column=2, sticky='nswe', pady=5, padx=5)

        tk.Grid.rowconfigure(upper_frame, 0, weight=1)
        tk.Grid.columnconfigure(upper_frame, 1, weight=1)

        mid_frame.pack(fill='both', expand=1, padx=10, pady=10)
        preview_text.pack(fill='both', expand=1)

        bottom_frame.pack(fill='x', padx=10, pady=5)
        save_button.pack(side='right', pady=5, padx=5)
        copy_button.pack(side='right', pady=5, padx=5)

if __name__ == '__main__':
    gui_ = GUI()
    gui_.run()