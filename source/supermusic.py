import requests
import re
import chord

def generate_text(http_link, transpose, out):
    output = generate_lyrics(http_link, transpose)
    _show_preview(output, out)

def generate_lyrics(htt_link, transpose):
    src = _get_source(htt_link)
    src = _strip_source_lyrics(src)
    return _format_source_lyrics(src, transpose)

def _get_source(http_link):
    req = requests.get(http_link)
    return req.text

def _strip_source_lyrics(src):
    raw_text = re.search(r"</SCRIPT>([\s\S^]*?)<script", src)
    raw_text = raw_text.group(1).strip()
    raw_text = re.sub(r"<sup>|</sup>", "", raw_text)
    return re.sub(r"<br/>", "§", raw_text)

def _format_source_lyrics(src, transpose):
    found_list = re.findall(r"<a.*?>.*?</a>", src)

    for item in found_list:
        chord_found = re.search(r"<a.*?>(.*?)</a>", item)
        chord_found = chord_found.group(1)
        final_chord = "[{}]".format(str(chord.Chord(chord_found).get_transposed(transpose)))
        src = src.replace(item, final_chord, 1)

    return src

def _show_preview(src, out):
    out.set_text(src)

def _write_file(src, filename):
    with open(filename, 'w') as f:
        f.write(src)

