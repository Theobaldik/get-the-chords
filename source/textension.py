import tkinter

class Varextension:

    def __init__(self):
        self.value = ""

    def set(self, value=""):
        self.value = value

    def get(self):
        return self.value

class Textension( tkinter.Frame ):
    """Extends Frame.  Intended as a container for a Text field.  Better related data handling
    and has Y scrollbar now."""

    def __init__( self, master, textvariable = None, *args, **kwargs ):
        self.textvariable = textvariable
        if ( textvariable is not None ):
            if not ( isinstance( textvariable, Varextension ) ):
                raise TypeError( "Varextension type expected, {} given.".format( type( textvariable ) ) )                        

        # build
        self.YScrollbar = None
        self.Text = None

        super().__init__( master )

        self.YScrollbar = tkinter.Scrollbar( self, orient = tkinter.VERTICAL )

        self.Text = tkinter.Text( self, yscrollcommand = self.YScrollbar.set, *args, **kwargs )
        self.YScrollbar.config( command = self.Text.yview )
        self.YScrollbar.pack( side = tkinter.RIGHT, fill = tkinter.Y )

        self.Text.pack( side = tkinter.LEFT, fill = tkinter.BOTH, expand = 1 )

    def clear( self ):
        self.Text.delete( 1.0, tkinter.END )

    def get_text( self ):
        text = self.Text.get( 1.0, tkinter.END )
        if ( text is not None ):
            text = text.strip()
        if ( text == "" ):
            text = None            
        return text

    def set_text( self, value ):
        self.clear()
        if ( value is not None ):
            rows = value.split('§')
            for row in rows:
                self.Text.insert( tkinter.END, row + '\n')