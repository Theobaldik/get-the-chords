# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Added

- Functions for generating author and title from [supermusic.cz](https://supermusic.cz/) website
- Functions for saving preview to file and copying to clipboard

### Fixed

- Code optimized

## [0.0] - 2020-03-29

### Added

- Functions for generating [ChordPro](https://www.chordpro.org/) lyrics format from [supermusic.cz](https://supermusic.cz/) website
- Basic GUI
- Initial setup

