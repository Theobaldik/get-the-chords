# Get the Chords

Get the Chords is free open source program for converting web based lyrics with chords to [ChordPro](https://www.chordpro.org/) format.

## Currently supported web servers for fetching:

- [supermusic.cz](https://supermusic.cz/)

## Installation

The program is self-contained. Everything you need is to [download](https://gitlab.com/Theobaldik/get-the-chords/-/raw/master/bin/dist/get-the-chords-0-0-0.exe?inline=false) the executable file and run it. Unfortunately we currently provide only Windows version.

## Using Get the Chords

Get the Chords is very easy to use. Just insert link to the lyrics, set your desired transposition and press the *Generate* button.

You can then edit the preview and save it or just copy it to clipboard.

![program-preview](resources/marketing/img/program-preview.png "Program preview")